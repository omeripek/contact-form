<?php
/**
 * Created by PhpStorm.
 * User: Ömer İPEK
 * Url www.omeripek.com.tr
 * Date: 05.03.2017
 * Time: 20:11
 */



if (!empty($_POST['email']) && $_SERVER["REQUEST_METHOD"] == "POST") {

    //date_default_timezone_set('Etc/UTC');
    date_default_timezone_set('Europe/Istanbul');

require 'vendor/phpmailer/phpmailer/PHPMailerAutoload.php';

function spamCheck($field){
    $field = filter_var($field, FILTER_SANITIZE_EMAIL);

    if(filter_var($field, FILTER_VALIDATE_EMAIL)){
        return $field;
    }else{
         http_response_code(400);
         exit;
    }
}

$email = spamCheck($_POST['email']);
$name = htmlspecialchars($_POST['name'], ENT_QUOTES,'UTF-8');
$subject = htmlspecialchars($_POST['subject'], ENT_QUOTES,'UTF-8');
$message = htmlspecialchars($_POST['message'], ENT_QUOTES,'UTF-8');

$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

//$mail->Debugoutput = 'html';


$mail->isSMTP();                                      // Set mailer to use SMTP
//$mail->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
$mail->Host = 'smtp-mail.outlook.com';  // Hotmail / Outlook

$mail->SMTPAuth = true;                               // Enable SMTP authentication
// $mail->Username = 'user@example.com';                 // SMTP username
$mail->Username = 'YOUR MAİL';                 // SMTP username
$mail->Password = 'YOUR PASSWD';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to

//$mail->setFrom('from@example.com', 'Mailer');
$mail->setFrom('YOUR MAİL', 'Test Mail');
$mail->addAddress('YOUR MAİL', 'Test Mail');     // Add a recipient
//$mail->addAddress('ellen@example.com');               // Name is optional
//$mail->addReplyTo('info@example.com', 'Information');

    //$mail->addReplyTo('sallamasyonhesap@hotmail.com.tr', 'Information');
//$mail->addCC('cc@example.com'); // if you need this you can  remove //
//$mail->addBCC('bcc@example.com');

//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
$mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = 'You Have Mail/Message From Contact Form!!'; // Your own mail subject


$mail->Body = <<<EOT
<b>Name:</b> {$name} <br />
<b>Email: </b>{$email} <br />
<b>Subject: </b>{$subject} <br />
<b>Message: </b> {$message} <br />
EOT;

    if(!$mail->Send()) {

        http_response_code(500);

    } else {

        http_response_code(200);
    }

} else {
    http_response_code(403);
}
